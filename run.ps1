
#now we need global variables, that we can use across all the steps and use as input arguments for each of the bicep templates./
param(
    [Parameter(Mandatory=$false)] [string] $rg = 'acc'
)
#$rg = "dev"

$vnet_address_space = '10.0.8.0/21'
$vnet_name = "$rg`_vnet"

$subnet1_range = '10.0.10.0/24'
$subnet1_name = 'vm_subnet'

$subnet2_range  = '10.0.11.0/24'
$subnet2_name = 'PLE_subnet'

$subnet3_range  = '10.0.12.0/26'
$subnet3_name = 'GatewaySubnet'

$nsg1_name  = 'vm_nsg'
$nsg2_name  = 'PLE_nsg'

$vnet_props = @{
    templateFile = ".\vnet.bicep"
    resourceGroupname = $rg
    vnet_address_space =$vnet_address_space
    vnet_name = $vnet_name
    subnet1_range = $subnet1_range
    subnet1_name = $subnet1_name
    subnet2_range = $subnet2_range
    subnet2_name = $subnet2_name
    subnet3_range = $subnet3_range
    subnet3_name = $subnet3_name
    nsg1_name = $nsg1_name
    nsg2_name = $nsg2_name
}

if(!(Get-AzResourceGroup -Name $rg)){
    Write-Error "Could not find the RG name $rg"
    exit -1
}

Write-Output "deploying vnet"
New-AzResourceGroupDeployment @vnet_props -Mode Incremental -verbose
