
param location string = resourceGroup().location
param vnet_name string 
param vnet_address_space string

param subnet1_range string 
param subnet1_name string

param subnet2_range string
param subnet2_name string

param subnet3_range string
param subnet3_name string

param nsg1_name string
param nsg2_name string


resource mbitvnet 'Microsoft.Network/virtualNetworks@2021-08-01' = {
  name: vnet_name
  location: location
  properties:{
    addressSpace: {
      addressPrefixes: [
        vnet_address_space
      ]
    }
    subnets: [
      {
        name: subnet1_name
        
        properties: {
          addressPrefix: subnet1_range
          serviceEndpoints: [
            {
              service: 'Microsoft.Storage'
              locations: [
                'westeurope'
                'northeurope'
              ]
            }
          ]
          delegations: []
          privateEndpointNetworkPolicies: 'Enabled'
          privateLinkServiceNetworkPolicies: 'Enabled'
        }
      
      }
      {
        name: subnet2_name
        properties: {
          addressPrefix: subnet2_range
          serviceEndpoints: [
            {
              service: 'Microsoft.Storage'
              locations: [
                'westeurope'
                'northeurope'
              ]
            }
          ]
          delegations: []
          privateEndpointNetworkPolicies: 'Enabled'
          privateLinkServiceNetworkPolicies: 'Enabled'
        }
      }
      {
        name: subnet3_name
        properties: {
          addressPrefix: subnet3_range
        }
      }

    ]
  }
}

resource nsg 'Microsoft.Network/networkSecurityGroups@2020-07-01' = {
  name: nsg1_name
  location: location
  dependsOn: [
    mbitvnet
  ]
}

resource nsgAttachment 'Microsoft.Network/virtualNetworks/subnets@2020-07-01' = {
  name: '${vnet_name}/${subnet1_name}'
  
  properties: {
    addressPrefix: subnet1_range
    networkSecurityGroup: {
      id: nsg.id
    }
  }
}

resource nsg2 'Microsoft.Network/networkSecurityGroups@2020-07-01' = {
  name: nsg2_name
  location: location
  dependsOn: [
    mbitvnet
  ]
}

resource nsgAttachment2 'Microsoft.Network/virtualNetworks/subnets@2020-07-01' = {
  name: '${vnet_name}/${subnet2_name}'
  properties: {
    addressPrefix: subnet2_range
    networkSecurityGroup: {
      id: nsg2.id
    }
  }
}


